var canvas = document.getElementById("myCanvas");
var body = document.getElementsByTagName("BODY")[0];
//var apikey = "AIzaSyBVyKkr4-Uxa5kRypYlDgYyrjPMD4yE3PI";//gmansour
var apikey = "AIzaSyCKzfJQIITxp-JSYq0jmoRG1RVyHJvJWtU"//gahmaresh
var lang = "fr";
var sheet = "1tnwMPLdxzq4JZc2C8VolLCIkZ4somoa6eDHfzOr_B-I";
//var engineID = "001502873696796661985:rgx-h-q_cbo" //gmansour.images
var engineID = "015235290089258307390:b5fnor6rmsg" //gahmaresh
var selectedIndex  = -1; //random
var headerRow=3//0based
var dict = {};
var settings = {};
var headers = [];
var input;
$(function() {
body.addEventListener("keypress", keypress);
input = document.createElement("input")
body.appendChild(input);
body.addEventListener("touchstart",tap);
$.getJSON("https://sheets.googleapis.com/v4/spreadsheets/"+sheet+"/values/"+lang+"?key="+apikey, function(data) {
	parse(data);
});
});
function parse(data)
{
	for (var i = 0 ; i < data.values[1].length; i++) 
	{
		settings[data.values[0][i].toLowerCase()]=data.values[1][i];
	};
	headers = data.values[settings.headerindex-1] //the headerindex is 1 bassed not 0 based
	for (var i = data.values.length - 1; i >= settings.headerindex; i--) {
		key =data.values[i][0].toUpperCase(); 
		dict[key]={};
		dict[key]["All"] = [];
		for (var j = data.values[i].length - 1; j >= 1; j--) 
		{
			val = data.values[i][j];
			if (val != null && val != "")
			{
				newarr = val.split("\n");
				dict[key][headers[j]]=newarr
				dict[key]["All"] = dict[key]["All"].concat(newarr);
			}
		};
	};
}
function tap (event) {
	input.focus();
}
function keypress(event) {
    window.speechSynthesis.cancel();
	var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
	var ctx=canvas.getContext("2d");

	ctx.beginPath();
	ctx.rect(0, 0, canvas.width, canvas.height);
	ctx.fillStyle = "white";
	ctx.fill();

	var color = "#"+Math.floor((Math.random() * 255) + 10).toString(16)+Math.floor((Math.random() * 255) + 10).toString(16)+Math.floor((Math.random() * 255) + 10).toString(16);
	var size = Math.floor((Math.random() * 300) + 100);
	var character = String.fromCharCode(chCode);
	var words = dict[character.toUpperCase()]
	if (words == null)
	{
		words = {};
	}
	var keyStr = words[headers[selectedIndex]];
	var word = "";
	if (selectedIndex == -1)
	{
		keyStr = "All";
	}
	words = words[keyStr];
	if (words && words.length > 0)
	{
		word = words[Math.floor((Math.random() * words.length) + 0)];
	}

	if (word != null && word != "")
	{
		var withformat = word.split(":");
		word = withformat[0];
		var q = word+" "+settings.searchprefix;
		if (withformat.length > 1 && withformat[1].startsWith("#"))
		{
			color = withformat[1];
			q = color;
		}
		else
		{
			$.getJSON("https://www.googleapis.com/customsearch/v1?q="+encodeURIComponent(q)+"&lr=lang_"+lang+"&cx="+engineID+"&num=10&safe=high&searchType=image&key="+apikey, function(data) {
				var img = new Image;
				img.onload = function(){
			    alpha = 0,          /// current alpha
			    delta = 0.01,        /// delta value = speed
					/// start loop
			    loop();
			    function loop() {
			        /// increase alpha with delta value
			        alpha += delta;
			        //// if delta <=0 or >=1 then reverse
			        if (alpha >= 1) return;
			        /// clear canvas
			        ctx.clearRect(0, 0, canvas.width, canvas.height);
			        /// set global alpha
			        ctx.globalAlpha = alpha;
			        /// re-draw image
				  	ctx.drawImage(img,0,0,canvas.width,canvas.height);
			        /// loop using rAF
			        requestAnimationFrame(loop);
			    }
				  var msg = new SpeechSynthesisUtterance(character.toLowerCase()+" "+(word?settings.isforstring+" "+word:""));
				  msg.lang = lang;
				  window.speechSynthesis.speak(msg);
				};
				var src = data.items[Math.floor((Math.random() * 10) + 0)].link;
				if (src == null)
					src = data.items[Math.floor((Math.random() * 10) + 0)].link; //try again!
				if (src != null)
				{
					img.src = src
				}
			});
		}
	}
	ctx.strokeStyle = 'black';
	ctx.font=""+size+"px Comic Sans MS";
	ctx.textAlign = "center";
	ctx.fillStyle = color;
	ctx.fillText(character.toUpperCase()
	, canvas.width/2, canvas.height/2);

	ctx.strokeText(character.toUpperCase()
	, canvas.width/2, canvas.height/2);

	var msg = new SpeechSynthesisUtterance(character.toLowerCase()+" ");
	msg.lang = lang;

    window.speechSynthesis.speak(msg);

//https://www.googleapis.com/customsearch/v1?q=folla&cx=001502873696796661985%3Argx-h-q_cbo&num=1&safe=high&key=
}